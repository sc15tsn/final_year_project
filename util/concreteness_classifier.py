import math
import os
import definitions
import random
import concreteness_data
import tensorflow as tf
import numpy as np
from gensim.models import KeyedVectors

WORD2VEC_PATH = os.path.join(definitions.RES_DIR, "GoogleNews-vectors-negative300.bin")

class ConcretenessClassifier:

	neural_net = None	#The network used in classification
	word2vec_model = None	#Model used for generating vectors from words

	def __init__(self, training_fract=0.7, num_hidden_layers=10, neurons_per_layer=10, given_steps=1000, debug=False):

		if debug:

			tf.logging.set_verbosity(tf.logging.INFO)
		else:

			tf.logging.set_verbosity(tf.logging.WARN)

		print "creating concreteness classifier with " + str(num_hidden_layers) + " x " + str(neurons_per_layer) + " hidden nodes, with " + str(given_steps) + " steps"
		print "loading word2vec model (this may take a while)..."

		#Load word2vec model
		self.word2vec_model = KeyedVectors.load_word2vec_format(WORD2VEC_PATH, binary=True)

		#Split into training and test instances
		training_instances, test_instances = self.getInstances(training_fract)

		#Create the feature columns
		feature_cols = [tf.feature_column.numeric_column("word_vec", shape=(1, 300), dtype=tf.float32)]

		#Create the layer size list
		layer_sizes = []

		for i in range(num_hidden_layers):

			layer_sizes.append(neurons_per_layer)

		#Create the network
		self.neural_net = tf.estimator.DNNClassifier(feature_columns=feature_cols, hidden_units=layer_sizes, n_classes=2)

		#Train the network
		self.train(training_instances, given_steps)

		#evaluate the network
		self.test(test_instances)

	def train(self, training_instances, given_steps):
		print "training concreteness classifier..."
		self.neural_net.train(input_fn=lambda: concreteness_data.createDataset(self.word2vec_model, training_instances), steps=given_steps)

	def test(self, test_instances):
		print "evaluating concreteness classifier..."
		self.neural_net.evaluate(input_fn=lambda: concreteness_data.createDataset(self.word2vec_model, test_instances), steps=1)

	def predictContinuous(self, word):

			#Convert the word to a vector and set up the input function
			sample = np.array([self.word2vec_model[word]])
			predict_input_fn = tf.estimator.inputs.numpy_input_fn(x={"word_vec": sample}, num_epochs=1, shuffle=False)

			#Make a prediction
			predictions = list(self.neural_net.predict(input_fn=predict_input_fn))

			#Return the probability distribution over the classes
			probabilities = predictions[0]['probabilities']
			class_index = np.argmax(probabilities)

			return probabilities

	def predictDiscrete(self, word):

			#Get the class probabilities
			probabilities = self.predictContinuous(word)
			class_index = np.argmax(probabilities)

			#Get the most likely, and return its name
			if class_index == 1:
				return "concrete"
			else:
				return "abstract"

	def getInstances(self, training_fract):

		#Get raw instances
		raw_instances = concreteness_data.getRawInstances()

		#Shuffle the instances
		random.shuffle(raw_instances)

		#Split instances based on training fract
		training_num = int(math.floor(len(raw_instances) * training_fract))
		training_instances = raw_instances[0:training_num]
		test_instances = raw_instances[training_num+1:]

		return training_instances, test_instances




