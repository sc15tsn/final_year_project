class clause_tree:

	#The tree, stored as a nested list
	#Clauses are stored as a tuple [Noun, Verb, Noun]
	#Subclauses are represented by attaching a subtree to a noun
	#i.e. [Noun, ... ] becomes [[Noun, [Clause word, [Subtree]]], ... ]

	tree = None

	def __init__(self, input_list, buildFromScratch):

		#If building from scratch, interpret the input list as a tagged sentence
		if buildFromScratch:

			#Build the tree from the given sentence, starting at the first word
			self.tree = self.createClauseTree(input_list, 0)[0]

		#Otherwise assume the tree has been given in list form
		else:

			self.tree = input_list

	def createClauseTree(self, tagged_sentence, word_offset):

		#Start by generating a blank list
		tree = ["*", "*", "*"]
		noun_pos = 0

		#Keep filling in the list until all entries are not blank
		while "*" in tree:

			#Get the current word
			current_word = tagged_sentence[word_offset]

			#Get the next word if possible
			if word_offset + 1 < len(tagged_sentence):
				next_word = tagged_sentence[word_offset + 1]

			else:
				next_word = [None, None]

			#If the word is a noun
			if current_word[1] == "N":

				#Determine which noun in the clause (subject/object) has been found
				if not tree[0] == "*":
					noun_pos = 2

				#Store the noun
				tree[noun_pos] = current_word

				#If the next word is a clause word, replace the noun with the subtree describing it recursively
				if next_word[1] == "C":

					#Call the algorithm starting on the next word
					subtree, new_offset = self.createClauseTree(tagged_sentence, word_offset + 1)

					#Replace the noun with the noun, clause word, and clause subtree
					tree[noun_pos] = [current_word, [next_word, subtree]]

					#And update where to look for the next word
					word_offset = new_offset

			#If the current word is a clause word
			elif current_word[1] == "C":

				#And the next word is a verb, then the parent noun is the subject, so mark it as so
				if next_word[1] == "V":
					tree[0] = "^"

			#If the current word is a verb
			elif current_word[1] == "V":

				#Store the verb in the verb position
				tree[1] = current_word

				#If the next word is a verb then we have reached the end of a clause of the form 'N that N V' so mark the parent noun as the object
				if next_word[1] == "V" or next_word[1] == None:
					tree[2] = "^"

			#Move on to the next pair of words
			if "*" in tree:
				word_offset = word_offset + 1

		return tree, word_offset

	def hasSubclause(self, word_index):

		#If the word indexed contains another list, then it has a subclause
		if isinstance(self.tree[word_index][0], list):
			return True
		else:
			return False

	#For each occurence of the word provided, specifies whether or not that word is within a subclause
	def isWithinSubclause(self, givenWord, listAll):

		result = []

		#Check the current clause
		for i in range(3):

			#If a word does not have a subclause, check to see if it matches the given word, and if so mark this occurence
			if not self.hasSubclause(i):

				if self.tree[i][0] == givenWord:

					result.append(False)

			else:

				#Same, but when the word does have a subclause
				if self.tree[i][0][0] == givenWord:

					result.append(False)

				#Check the subclause for occurrences of the word
				subClauseTree = self.getSubclause(i)
				subResult = subClauseTree.isWithinSubclause(givenWord, listAll)

				#Mark each occurence as false, as it was within a subclause
				for j in range(len(subResult)):

					subResult[j] = True

				result.extend(subResult)

		return result

	def isSubject(self, givenWord, listAll):

		result = []

		for i in range(3):

			#Ignore references to a higher layer of the tree
			if self.tree[i] == "^":
				continue

			#Case when word has no subclause
			if not self.hasSubclause(i):

				print "Doesn't have subclause"

				#If word is a noun that matches the given word, and is in the 1st pos of the clause, it is a subject
				if self.tree[i][1] == "N" and self.tree[i][0] == givenWord:

					if i == 0:

						result.append(True)

					elif i == 2:

						result.append(False)

			else:

				#Same, but for words that have a subclause
				if self.tree[i][0][1] == "N" and self.tree[i][0][0] == givenWord:

					if i == 0:

						result.append(True)

					elif i == 2:

						result.append(False)

				subClauseTree = self.getSubclause(i)
				subResult = subClauseTree.isSubject(givenWord, listAll)
				result.extend(subResult)

		return result

	def isTheme(self, givenWord, listAll):

		result = []

		for i in range(3):

			#Ignore references to higher layers of tree
			if self.tree[i] == "^":

				continue

			#Case when word has no subclause
			if not self.hasSubclause(i):

				#If word is a noun that matches the given word, and is in the last pos of the clause, it is a subject
				if self.tree[i][1] == "N" and self.tree[i][0] == givenWord:
					
					if i == 2:

						result.append(True)

					elif i == 0:

						result.append(False)

			else:

				#Same, but for words that have a subclause
				if self.tree[i][0][1] == "N" and self.tree[i][0][0] == givenWord:

					if i == 2:

						result.append(True)

					elif i == 0:

						result.append(False)

				subClauseTree = self.getSubclause(i)
				subResult = subClauseTree.isTheme(givenWord, listAll)
				result.extend(subResult)

		return result

	def getSubclause(self, word_index):

		return clause_tree(self.tree[word_index][1][1], False)

	def getClauseWord(self, word_index):

		return self.tree[word_index][1][0]

	def printTree(self):

		self.printPartialTree(0)

	def printPartialTree(self, indents):

		#Print the element (with indents if necessary)
		for i in range(3):

			if not self.hasSubclause(i):

				string = (" " * 3 * indents) + self.tree[i][0]
				print string

			#If there is a subclause
			else:

				#Print the word with the indents
				string = (" " * 3 * indents) + self.tree[i][0][0]
				print string

				#Print the clause word on the next line with an extra indent
				string = (" " * 3 * (indents + 1)) + self.getClauseWord(i)[0]
				print string

				#Then get the subclause and print it out recursively with another indent
				self.getSubclause(i).printPartialTree(indents + 2)
