from gensim.models import KeyedVectors
import os
import definitions
import numpy as np
import tensorflow as tf
import random
import math

DATA_PATH = os.path.join(definitions.RES_DIR, "concreteness_ratings.txt")

def getRawInstances():

	#Extract the raw data from the training fileS
	with open(DATA_PATH, "r") as datafile:

		#Skip over headings
		datafile.readline()
		instances = []

		#Extract instances
		for line in datafile:

			#Extract features
			features = line.split("\t")

			#Ignore bigrams
			if int(features[1]) == 1:
				continue

			#Extract the word and concreteness
			instances.append([features[0], features[2]])

		print("extracted " + str(len(instances)) + " instances.")
		return instances

def createDataset(model, raw_instances):

	features = {}
	features['word_vec'] = []
	labels = []

	#Iterate through each instance
	for instance in raw_instances:

		#Get the word vector for that word, skipping over words that are not in the vocabulary
		try:
			word_vec = tf.convert_to_tensor(model[instance[0]])

		except KeyError:
			continue

		#Add the word vector to the feature list
		features['word_vec'].append(word_vec)

		#Get the class for each instance
		if float(instance[1]) < 2.5:
			labels.append(0) #0 = abstract
		else:
			labels.append(1) #1 = concrete

	#Convert to tensors
	features['word_vec'] = tf.convert_to_tensor(features['word_vec'])
	labels = tf.convert_to_tensor(labels)

	return features, labels
