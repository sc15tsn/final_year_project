import os

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
RES_DIR = os.path.join(ROOT_DIR, "res")
UTIL_DIR = os.path.join(ROOT_DIR, "util")
CORE_DIR = os.path.join(ROOT_DIR, "core")
