import sys
import definitions
import nltk
sys.path.append(definitions.UTIL_DIR)
from clause_tree import clause_tree

#Checks to see if an item matches at least one element in a list
def matchesOne(item, list):

	for current_item in list:

		if(item == current_item):

			return True

	return False

def determineTag(tagged_word):

	nouns = ["NN", "NNS", "NNP", "NNPS"]
	verbs = ["VB", "VBD", "VBG", "VBN", "VBP", "VBZ"]
	clause = ["WP", "WDT", "WRB"]

	if matchesOne(tagged_word[1], nouns):

		tagged_word[1] = "N"

	elif matchesOne(tagged_word[1], verbs):

		tagged_word[1] = "V"

	elif matchesOne(tagged_word[1], clause) or (tagged_word[1] == "IN" and tagged_word[0] == "that"):

		tagged_word[1] = "C"

	else:

		tagged_word[1] = "O"

if __name__ == "__main__":

	print "Enter sentences to generate a clause tree:"
	input_sent = ""

	while not input_sent == "\n": 

		input_sent = raw_input()
		words = nltk.word_tokenize(input_sent)
		pos_tagged = nltk.pos_tag(words)

		new_pos_tagged = []

		#Convert tuples to lists
		for word in pos_tagged:
			new_pos_tagged.append(list(word))

		#Simplify tags
		for word in new_pos_tagged:
			determineTag(word)

		#Discard unnecessary tags
		words_to_remove = []

		for tagged_word in new_pos_tagged:

			if tagged_word[1] == "O":

				words_to_remove.append(tagged_word)

		for word in words_to_remove:

			new_pos_tagged.remove(word)

		#Build and display clause tree
		tree = clause_tree(new_pos_tagged, True)
		tree.printTree()