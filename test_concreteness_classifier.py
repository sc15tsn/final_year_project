import sys
import definitions
sys.path.append(definitions.UTIL_DIR)
from concreteness_classifier import ConcretenessClassifier

conc = ConcretenessClassifier()
print "setup complete. enter words to test their 'abstractness'"

input_word = ""

#Read in words and predict their concreteness until nothing is entered
while not input_word == "\n":

	input_word = raw_input()

	try:
		probs = conc.predictContinuous(input_word)

	except KeyError:
		print "Word not found in model vocabulary"
		
	else:
		print "Abstract: " + str(probs[0] * 100) + "%, Concrete: " + str(probs[1] * 100) + "%"