Installation
---

1. Make sure python 2.x.x and pip are installed
1. Navigate to the cloned repository
3. Install dependencies
```sh
pip install -r requirements.txt
```
4. Run python in the shell and enter the following commands
```python
import nltk
nltk.download('punkt')
nltk.download('averaged_perceptron_tagger')
```
5. Navigate to the res directory
```sh
cd res
```
6. Unzip the word2vec model (This may take a while!)
```sh
gzip -d GoogleNews-vectors-negative300.bin.gz
```

Running Clause Tree Generation
---

Running Abstractness Classifier
---

Running Binding Neural Network
---


Attribution
---

Repository icon made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>
