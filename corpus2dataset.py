import os
import nltk
import sys
import definitions
import numpy as np
import tensorflow as tf
sys.path.append(definitions.UTIL_DIR)
from nltk.parse import stanford
from clause_tree import clause_tree
from concreteness_classifier import ConcretenessClassifier

def getPOSTaggedSents(corpus):

	#Split the corpus into sentences
	sentences = nltk.sent_tokenize(corpus)
	tagged_sentences = []

	#Get POS tags for the words in each sentence.
	for sentence in sentences:

		words = nltk.word_tokenize(sentence)
		pos_tagged = nltk.pos_tag(words)

		new_pos_tagged = []

		#Convert tuples to lists
		for word in pos_tagged:
			new_pos_tagged.append(list(word))

		tagged_sentences.append(new_pos_tagged)

	return tagged_sentences

#Checks to see if an item matches at least one element in a list
def matchesOne(item, list):

	for current_item in list:

		if(item == current_item):

			return True

	return False

def determineTag(tagged_word):

	nouns = ["NN", "NNS", "NNP", "NNPS"]
	verbs = ["VB", "VBD", "VBG", "VBN", "VBP", "VBZ"]
	clause = ["WP", "WDT", "WRB"]

	if matchesOne(tagged_word[1], nouns):

		tagged_word[1] = "N"

	elif matchesOne(tagged_word[1], verbs):

		tagged_word[1] = "V"

	elif matchesOne(tagged_word[1], clause) or (tagged_word[1] == "IN" and tagged_word[0] == "that"):

		tagged_word[1] = "C"

	else:

		tagged_word[1] = "O"

def discardUnnecessaryTags(tagged_corpus):

	words_to_remove = []

	for tagged_sentence in tagged_corpus:

		words_to_remove = []

		for tagged_word in tagged_sentence:

			if tagged_word[1] == "O":

				words_to_remove.append(tagged_word)

		for word in words_to_remove:

			tagged_sentence.remove(word)

	return tagged_corpus

#Takes the output of nltk POS tagging and simplifies the tags to make them more suitable for dataset generation
def simplifyCorpusTags(tagged_corpus):

	for tagged_sentence in tagged_corpus:

		for tagged_word in tagged_sentence:

			determineTag(tagged_word)

	return tagged_corpus

def countTag(tagged_sentence, tag):

	count = 0

	for tagged_word in tagged_sentence:

		if tagged_word[1] == tag:

			count = count + 1

	return count

def removeInvalidSentences(tagged_sents):

	sents_to_remove = []

	for sentence in tagged_sents:

		isEmpty = len(sentence) == 0

		if isEmpty:

			sents_to_remove.append(sentence)
			continue

		hasNoNouns = countTag(sentence, "N") == 0
		hasNoVerbs = countTag(sentence, "V") == 0
		hasCorrectNumber = countTag(sentence, "V") < countTag(sentence, "N")
		startsWithNoun = sentence[0][1] == "N"
		endsCorrectly = sentence[len(sentence) - 1][1] == "N" or sentence[len(sentence) - 1][1] == "V"

		if hasNoNouns or hasNoVerbs or not startsWithNoun or not hasCorrectNumber or not endsCorrectly:

			sents_to_remove.append(sentence)
			continue

		for i in range(len(sentence) - 1):

			current_word = sentence[i]
			next_word = sentence[i + 1]

			#Make sure nouns are followed by clause words or verbs
			if current_word[1] == "N":

				if not next_word[1] == "C" and not next_word[1] == "V":

					sents_to_remove.append(sentence)
					break

			#Make sure clause words and verbs are followed by nouns or verbs
			if current_word[1] == "C" or current_word[1] == "V":

				if not next_word[1] == "N" and not next_word[1] == "V":

					sents_to_remove.append(sentence)
					break

	for sent in sents_to_remove:

		tagged_sents.remove(sent)

	return tagged_sents


def generateDataset(concrete_classifier, corpus, debug=False):

	tf.logging.set_verbosity(tf.logging.WARN)

	features = {}
	features['input_signals'] = []
	labels = []

	infile = open(corpus, "r")
	corpus_string = infile.read()

	print "extracting corpus data from \'" + corpus + "\'..."
	tagged_sents = getPOSTaggedSents(corpus_string) #Apply part of speech tagging to corpus
	sents_simplified = simplifyCorpusTags(tagged_sents) #Simplify tags - group nouns and verbs, etc...
	sents_simplified = discardUnnecessaryTags(sents_simplified) #Remove 'O' tags
	final_sents = removeInvalidSentences(sents_simplified) #Remove sentences in incorrect format

	print "generating clause trees..."
	#Generate clause tree for each sentence
	clause_trees = []

	for sent in final_sents:

		tree = clause_tree(sent, True)
		clause_trees.append(tree)

	print len(clause_trees)

	print "generating binding examples..."
	#Generate the dataset examples a sentence at a time
	for sent_info in zip(final_sents, clause_trees):

		inputs, outputs = generateSentenceDataset(concrete_classifier, sent_info[0], sent_info[1], debug=debug)

		#Pair up input/output pairs and add the vectors to the list of features/labels
		for dataset_example in zip(inputs, outputs):

			features['input_signals'].append(tf.convert_to_tensor(dataset_example[0]))

			#Encode output vector as decimal value and store as label
			labels.append(bitVecToDecimal(dataset_example[1]))

	features['input_signals'] = tf.convert_to_tensor(features['input_signals'])
	labels = tf.convert_to_tensor(labels)

	tf.logging.set_verbosity(tf.logging.INFO)
	close(infile)

	return features, labels


def generateSentenceDataset(concrete_classifier, tagged_sent, clause_tree, debug=False):

	#A dict for each unique word indicating which occurence of this word we are currently at
	occurences = {}

	#Lists for storing input and output mappings for each word
	inputs = []
	outputs = []

	#Initialise the dictionary
	for word in tagged_sent:

		occurences[word[0]] = 0

	#Prev output used for copying feedback - initially set to zero
	prev_output = np.zeros(13)

	#Iterate through each word
	for i in range(len(tagged_sent)):

		tagged_word = tagged_sent[i]

		#Input
		#[0,   1,   2, 3, 4,  5,  6, 7]
		# Nrc, Ncc, V, C, RC, CC, T, Cv

		#Output
		#[0,     1,     2,     3,     4,   5,   6,   7,   8,   9,  10, 11, 12]
		# N-n-S, V-v-S, C-c-N, N-n-C, V-t, N-t, C-t, C-v, V-v, RC, CC, T,  Cv

		input = np.zeros(8)
		output = np.zeros(13)

		#If this is not the first word, copy the feedback from the previous word to the input of this one
		if not i == 0:

			input[4:] = prev_output[9:]

		#If this is a noun, use the concreteness classifier to predict the type, and mark the corresponding bit in the input
		if tagged_word[1] == "N":

			classification = concrete_classifier.predictDiscrete(tagged_word[0])

			if classification == "concrete":
				input[0] = 1
				output[9] = 1

			elif classification == "abstract":
				input[1] = 1
				output[10] = 1

			#If the noun is a theme, open the theme subassembly (N-t)
			if clause_tree.isTheme(tagged_word[0], True)[occurences[tagged_word[0]]]:

				output[5] = 1

			#Else if it is a subject
			else:

				#If it is not within a subclause, mark as the subject of the entire sentence (N-n-S)
				if not clause_tree.isWithinSubclause(tagged_word[0], True)[occurences[tagged_word[0]]]:

					output[0] = 1

				#If it is within a subclause, bind as the subject of the clause
				else:

					output[3] = 1

					#If it is a relative clause, then also open the clause's theme subassembly
					if input[4] == 1:

						output[4] = 1


		#Otherwise mark the corresponding word type bit
		elif tagged_word[1] == "V":

			input[2] = 1

			#Open the theme subassembly for binding (V-t), and indicate that a theme is expected (T)
			output[4] = 1
			output[11] = 1

			#If the verb is not within a subclause, mark as the main verb of the sentence (V-v-S)
			if not clause_tree.isWithinSubclause(tagged_word[0], True)[occurences[tagged_word[0]]]:

				output[1] = 1

			#If it is within a subclause, bind to the clause word (V-v)
			else:

				output[8] = 1

		elif tagged_word[1] == "C":

			input[3] = 1

			#Bind clause word to noun (C-c-N), open verb subassembly for binding (C-v) and indicate so (Cv), and copy clause type from input (RC/CC)
			output[2] = 1	#C-c-N
			output[7] = 1	#C-v
			output[12] = 1	#Cv

			#RC/CC, only one can be active at a time, so copying both is sufficient
			output[9] = input[4]
			output[10] = input[5]


		if debug:

			print tagged_word[0]
			print input
			print output
			print "\n"

		inputs.append(input)
		outputs.append(output)

		#Remember the output for use in the next prediction
		prev_output = output

		#Update the number of occurences of this word
		occurences[tagged_word[0]] += 1

	return inputs, outputs

def bitVecToDecimal(bit_vector):

	decimal = 0

	for bit_index in range(len(bit_vector)):

		if bit_vector[bit_index] == 1:

			decimal = decimal + (2**(len(bit_vector) - 1 - bit_index))

	return decimal
