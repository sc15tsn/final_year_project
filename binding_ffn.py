import math
import os
import sys
import definitions
import corpus2dataset
import tensorflow as tf
import numpy as np
sys.path.append(definitions.UTIL_DIR)
from concreteness_classifier import ConcretenessClassifier

class BindingNeuralNetwork:

	neural_net = None	#The network used in classification
	conc_classif = None

	def __init__(self, concreteness_classif, training_file, test_file, num_hidden_layers, neurons_per_layer, given_steps, debug=False):

		if debug:

			tf.logging.set_verbosity(tf.logging.INFO)

		else:

			tf.logging.set_verbosity(tf.logging.WARN)

		print "creating binding ffn with " + str(num_hidden_layers) + " x " + str(neurons_per_layer) + " hidden nodes, with " + str(given_steps) + " steps"

		self.conc_classif = concreteness_classif

		#Create the feature columns
		feature_cols = [tf.feature_column.numeric_column("input_signals", shape=(1, 8), dtype=tf.float32)]

		#Create the layer size list
		layer_sizes = []

		for i in range(num_hidden_layers):

			layer_sizes.append(neurons_per_layer)

		#Create the network
		self.neural_net = tf.estimator.DNNClassifier(feature_columns=feature_cols, hidden_units=layer_sizes, n_classes=8192)

		#Train the network
		self.train(training_file, given_steps)

		#Evaluate the network
		self.test(test_file)

	def train(self, training_file, given_steps):
		print "training binding ffn..."
		self.neural_net.train(input_fn=lambda: corpus2dataset.generateDataset(self.conc_classif, training_file), steps=given_steps)

	def test(self, test_file):
		print "evaluating binding ffn..."
		return self.neural_net.evaluate(input_fn=lambda: corpus2dataset.generateDataset(self.conc_classif, test_file), steps=1)

if __name__ == "__main__":

	conc = ConcretenessClassifier()
	bnn = BindingNeuralNetwork(conc, "res/training_corpus.txt", "res/test_corpus.txt", 10, 10, 1000)